#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "prog1_2.h"


int countTokens(char *str) /*takes given string and counts character groups separated by whitespace*/
{
    int slen;
    int count;
    char countBuff[256];
    for(count=0;sscanf(str,"%s%n",countBuff,&slen) != -1;count++)
    {
        str = str + slen;
    }
    return count;
}


char** getTokens(char *str) /*takes given string and separates whitespace-delimited groups of characters into an array*/
{
    char **vals = (char**) malloc(sizeof(char*) * countTokens(str));
    char *getBuff = (char*) malloc(sizeof(char)*256);
    int slen;
    int count;
    for(count=0;sscanf(str,"%s%n",getBuff,&slen) != -1;count++)
    {
        str = str + slen;
        vals[count]=(char*) malloc(sizeof(char)*(slen+1));
        strcpy(vals[count],getBuff);
    }
    return vals;
}


void main(int argc, char*argv[]) /*program to use stack functions on command line*/
{
	printf("Assignment #1-3, Connor Campi, connor@campi.cc\n");
	if(argc != 2) /*command line takes 1 argument: the number of stack operations desired*/
	{
	    printf("This program expects a single command line argument.");
	    return;
	}
	
	STACK *input = MakeStack(10);
	char buff[256];
	for(int i = 0; i < atoi(argv[1]); i++) /*parses command line input into stack operations*/
	{
	    printf("> ");
        fgets(buff,256,stdin);
	    int numTok = countTokens(buff);
	    char **tokens = getTokens(buff);
	    if((numTok == 2) && strcmp(tokens[0],"push") == 0)
	    {
	        Push(input, atoi(tokens[1]));
	    }
	    
	    if((numTok == 1) && strcmp(tokens[0],"pop") == 0)
	    {
	        printf("%d\n",Pop(input));
	    }
	}
}