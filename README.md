Connor Campi

connor@campi.cc

prog1_1.c prompts the user to input their name, then greets them.

prog1_2.h creates function header definitions for a class called STACK.

prog1_2.c defines those functions created in prog1_2.h, turning a STACK into a data structure that can grow to hold as many objects as it needs to, and operates on a first-in, last-out basis.

prog1_3.c is a program that allows the user to, through the command line, make use of the STACK functions defined in prog1_2.