#include <stdlib.h>
#include "prog1_2.h"
#include <string.h>
STACK* MakeStack(int initialCapacity)
{
	STACK *NEWSTACK = malloc(sizeof(STACK));
	NEWSTACK->capacity = initialCapacity;
	NEWSTACK->size = 0;
	NEWSTACK->data = malloc(initialCapacity * sizeof(int));
	return NEWSTACK;
}

void Push(STACK *stackPtr,int data) /*takes specified stack and inserts given data*/
{
	if(stackPtr->capacity == stackPtr->size)
	{
		Grow(stackPtr);
	}

	memcpy(stackPtr->data + ( stackPtr->size * sizeof(int) ) , &data, sizeof(int));
	stackPtr->size = stackPtr->size + 1;
}

int Pop(STACK *stackPtr) /*takes given stack, removing and returning data on top*/
{
	int popped;
	if(stackPtr->size == 0)
	{
		return -1;
	}
	memcpy(&popped, stackPtr->data + ( ( stackPtr->size -1 ) * sizeof(int) ) , sizeof(int));
	stackPtr->size = stackPtr->size - 1;
	return popped;
}

void Grow(STACK *stackPtr) /*increases stack capacity by 2x*/
{
	stackPtr->capacity *= 2;
	stackPtr->data = realloc(stackPtr->data, stackPtr->capacity * sizeof(int));
} 
